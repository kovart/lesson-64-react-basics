import React, {Fragment, useState} from 'react'
import './HelloApp.scss'
import className from 'classname'

// Class component
export class BasicApp extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            value: "Make active",
            isActive: false
        }
        // If we don't bind context, "this" inside this function will be "undefined"
        this.isActiveToggle = this.isActiveToggle.bind(this)
    }

    isActiveToggle() {
        // this.state.value = 12
        // we cannot change the state without calling this.setState() function

        this.setState({
            value: !this.state.isActive ? "Make inactive" : "Make active",
            isActive: !this.state.isActive
        })
    }

    render() {
        const classes = className("App", {"App--active": this.state.isActive})

        return (
            <Fragment>
                <div className={classes}>
                    <button onClick={this.isActiveToggle}>
                        {this.state.value}
                    </button>
                </div>
                <br/>
                {this.state.isActive && <AppWithHooks someValue="SomeText"/>}
            </Fragment>
        )
    }
}

// Pure functional component with React Hooks
// ----
// A pure function is a function where the return value
// is only determined by its input values (props), without observable side effects (without async changes)
export function AppWithHooks({someValue}) {
    // const [x, y] = [1, 2] --> the same as below
    const [isActive, setIsActive] = useState(false) // -> [value, setValue()]

    const value = isActive ? "Make inactive" : "Make active"
    const classes = className("App", {"App--active": isActive})

    return (
        <div className={classes}>
            {someValue}
            <button onClick={() => setIsActive(!isActive)}>
                {value}
            </button>
        </div>
    )
}
