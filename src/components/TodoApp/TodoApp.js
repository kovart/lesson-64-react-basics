import React, {useState} from "react"
import "./TodoApp.scss"

function TodoApp() {
    const [items, setItems] = useState(["item1", "item2"])
    const [itemName, setItemName] = useState("")

    return (
        <div className="TodoApp">
            <input className="TodoApp__input" type="text" placeholder="What will you want to do?" onChange={e => setItemName(e.target.value)}/>
            {items && <ul className="TodoApp__item-list">
                {items && items.map(item => (
                    <div className="item" key={item}>
                        <div className="item__name">{item}</div>
                        <button className="item__remove-btn">-</button>
                    </div>
                ))}
            </ul>}
        </div>
    )
}

export default TodoApp
